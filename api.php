<?php
class API{
    public function printName($fullName){
        echo "Full Name: " .$fullName. "\n";
    }

    public function printHobbies($hobbies){
        echo "Hobbies: \n";
        foreach($hobbies as $hobby){
            echo " ".$hobby."\n";
        }
    }

    public function printInformation($info){
        foreach($info as $inf => $value){
            if($inf == "Birthday:"){
                $value = date("F j, Y", strtotime($value));
            }
            echo $inf. " ", $value."\n";
        }
    }

}

$api = new API();
$api->printName("Kenneth Emmanuel N. Benibe");
$api->printHobbies(array("Gaming", "Exploring", "Driving"));
$api->printInformation((object)array("Age:"=>"21", "Email:"=>"kennethemmanuel.benibe.pixel8@gmail.com", "Birthday:"=>"2001-06-08"));

?>